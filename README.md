# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Git Training

### How do I get set up? ###

* python -m venv .venv
* source .venv/bin/activate

### Workflow guidelines ###

* Create feature branch (feature/<description>)
* Commit changes with proper descriptions
* push --set-upstream origin <branchname>
* Create pull req
* Merge changes

###
### Who do I talk to? ###

* Per-Morten Lindanger
